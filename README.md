# mxGage 

I am going to start with building or attempting to build a Framework for our next project.

I will be using Symfony3 components to accomplish what I think the Blog/CMS core should be.



## Planned Features

* i18n Ready
* Skins/Zhems - System
* Plugin or Module system 
* Online update/install method
* EU Cookie Law Compliance
* No theme dependance
* Captcha and/or reCaptcha
* Bootstrap
* JQuery
* User 
  * Login
  * Logout
* Admin 
  * Backend
  * Login
  * Logout

## Requirements

* MySQL (using symfony componets here)
* PHP 7 +
* Webserver


## Documentation

Most at the [HubShark](http://hubshark.com)



## Bugs, Issues, Requests, etc.

[Go here](https://gitlab.com/MustangX/mxCage/wikis/home)


